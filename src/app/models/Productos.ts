export interface Producto{
    id?: number;
    name?: string;
    price?: number;
    quantity?: number
    image?: string;
    create_at?: Date;
}