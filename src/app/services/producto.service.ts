import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Producto} from '../models/Productos'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  API_URI = 'http://10.104.5.54:3000/api'
  token:any = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTY2MzUxMzM5fQ.dz0V50MQ69-V8ykWymDbf89aBjJvElPO1lBZkgehHvY'
  header = {headers: {'Authorization': 'Bearer '+ this.token}}

  constructor(private http: HttpClient) { }

  getProductos(){
    return this.http.get(`${this.API_URI}/productos`);
  }

  getProducto(id: number){
    return this.http.get(`${this.API_URI}/productos/${id}`, this.header);
  }

  saveProducto(producto: Producto){
    return this.http.post(`${this.API_URI}/productos`, producto, this.header);
  }

  deleteProducto(id: string){
    return this.http.delete(`${this.API_URI}/productos/${id}`, this.header);
  }

  updateProducto(id: number, updateProducto: Producto): Observable<Producto>{
    return this.http.put(`${this.API_URI}/productos/${id}`,updateProducto , this.header);
  }
}
