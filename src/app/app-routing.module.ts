import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ProductoListComponent} from './components/producto-list/productos-list.component';
import {ProductosFormComponent} from './components/producto-form/producto-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/productos',
    pathMatch: 'full'
  },
  {
    path: 'productos',
    component: ProductoListComponent
  },
  {
    path: 'productos/add',
    component: ProductosFormComponent
  },
  {
    path: 'productos/edit/:id',
    component: ProductosFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
