import { Component, OnInit, HostBinding } from '@angular/core';
import { ProductosService } from '../../services/producto.service'
  import { from } from 'rxjs';

@Component({
  selector: 'app-game-list',
  templateUrl: './productos-list.component.html',
  styleUrls: ['./productos-list.component.css']
})
export class ProductoListComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  productos:any = [];
  constructor(private productosService: ProductosService) { }

  ngOnInit() {
    this.getProductos();
  }
  
  getProductos(){
    this.productosService.getProductos().subscribe(
      res => {
        this.productos = res;
        this.productos = JSON.parse(this.productos);
        console.log(this.productos);
      } ,
      err => console.log(err)
    )
  }

  deleteProducto(id: string){
    this.productosService.deleteProducto(id).subscribe(
      res => {
        console.log(res)
        this.getProductos();
      },
      err => console.log(err)
    )
  }

}
