import { Component, OnInit, HostBinding } from '@angular/core';
import { Producto } from 'src/app/models/Productos';
import { Router, ActivatedRoute } from '@angular/router'

import { ProductosService } from '../../services/producto.service'

@Component({
  selector: 'app-producto-form',
  templateUrl: './producto-form.component.html',
  styleUrls: ['./producto-form.component.css']
})
export class ProductosFormComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  producto: Producto = {
    id: 0,
    name: '',
    price: 0,
    quantity: 0,
    image: '',
    create_at: new Date()
  };

  edit: boolean = false;

  constructor(private produtoService: ProductosService, private router: Router, private activeteroute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activeteroute.snapshot.params;
    console.log(params);
    if(params.id){
      this.produtoService.getProducto(params.id)
        .subscribe(
          res => {
            console.log(res);
            this.producto = res;
            this.edit = true;
          },
          err => console.log(err)
        )
    }
  }

  saveNewProducto (){
    delete this.producto.create_at;
    delete this.producto.id;
    this.produtoService.saveProducto(this.producto)
    .subscribe(
      res=>{
        console.log(res)
        this.router.navigate(['/productos']);
      },
      err => console.log(err)
    )
  }
  updateProducto (){
    delete this.producto.create_at;
    this.produtoService.updateProducto(this.producto.id, this.producto)
    .subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/productos']);
      },
      err => console.log(err)
    )
  }

}
