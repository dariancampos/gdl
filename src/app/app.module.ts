import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http'
import {FormsModule} from '@angular/forms'


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ProductosFormComponent } from './components/producto-form/producto-form.component';
import { ProductoListComponent } from './components/producto-list/productos-list.component';

import { ProductosService } from './services/producto.service'
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ProductosFormComponent,
    ProductoListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ProductosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
